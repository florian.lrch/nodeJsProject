//Controleur de la page d'acceuil
let model = require('../models/home.js'); //On fait appel au model home.js (qui contient toutes les requêtes SQL)

//Dans le cas ou la page n'est pas indexé alors on renvoie sur notFound
module.exports.NotFound = function(request, response){
    response.title = "Bienvenue sur le site de SIXVOIX (IUT du Limousin).";
    response.render('notFound', response);
};

//Page d'acceuil de l'application
module.exports.Index = function(request, response){
    response.title = "Bienvenue sur le site de WROOM (IUT du Limousin).";
    //On récupère le dernier résultat
    model.getDernierResult( function (err, result) {
       if (err) {
           // gestion de l'erreur
           console.log(err);
           return;
       }
       response.dernierResult = result[0];
       response.render('home', response); //On le retourne dans la vue home
     });

};
