//Controleur des résultats
let async = require('async'); //Permet l'éxécution des fonctions de manière asynchrones
let model = require('../models/resultat.js');	//On fait appel au model resultat.js (qui contient tous les requêtes SQL)

//Affichage de la liste des grands prix
module.exports.ListerResultat = function(request, response){
	response.title = 'Liste des résulats des grands prix';

	//On récupère la liste des grands prix dans le model
	model.getListeGrandPrix( function (err, result) {
    if (err) {
      // gestion de l'erreur
      console.log(err);
      return;
    }
    response.listeGrandPrix = result;
    //console.log(result);
    response.render('listerResultat', response);	//On renvoie la réponse dans la vue partielle listerResultat
  });
};

//On affiche les détail du grands prix sélectionné
module.exports.DetailGrandPrix = function(request, response){
  response.title = 'Détails du grand prix';
	let numGrandPrix = request.params.numGrandPrix;	//On récupère le numéro du grand prix passé dans l'URL

  async.parallel([
    function (callback){
      model.getListeGrandPrix( function (err, result){callback(null,result)});	//On récupère la liste des gp pour la vue partielle
    },
    function (callback){
      model.getDetailGrandPrixById( numGrandPrix, (function (err, result){callback(null,result)}));	//On récupère les détails sur un grand prix à partir de son numéro
    },
		function (callback){
			model.getTempsPilote( numGrandPrix, (function (err, result){callback(null,result)}));	//On récupère le temps des pilotes du gp
		},
		function (callback){
			model.getPlaceNbPoint( function (err, result){callback(null,result)});	//On récuère le nombre de points par place au classement
		},
  ],
  function (err, result){
    if (err) {
      console.log(err);
      return;
    }
    response.listeGrandPrix = result[0];
    response.detailGrandPrix = result[1][0];
		response.resultat = recupElements();

		//Permet de créer un unique tableau facilitant l'affichage
		function recupElements() {
			let tableauResultat = [];
			let i = 0;
			while (i < (result[2].length) && i < 10) {	//Tant qu'il y a des pilotes et que leur nombre est infèrieur à 11 (car de 0 à 10)
				tableauResultat[i] = new Object;	//On créer un nouvel objet pour notre tableau à la position donnée
				//On y met les informations
				tableauResultat[i].place = result[3][i]['ptplace'];
				tableauResultat[i].pilnom = result[2][i]['PILNOM'];
				tableauResultat[i].temps = result[2][i]['TEMPSCOURSE'];
				tableauResultat[i].point = result[3][i]['ptnbpointsplace'];
				i++;
			}
			return tableauResultat;	//On retourne notre tableau de résultats
		}
    response.render('detailGrandPrix', response);
  }
);
};
