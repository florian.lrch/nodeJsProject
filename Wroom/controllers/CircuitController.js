//Controleur des circuits

let async = require('async'); //Permet l'éxécution des fonctions de manière asynchrones
let model = require('../models/circuit.js');  //On fait appel au model circuit.js (contient toutes les requêtes SQL)


//Affichage de la liste des circuits dans la vue partielle
module.exports.ListerCircuit = function(request, response){
    response.title = 'Liste des circuits';

    //On fait appel au model pour exécuter la requête SQL
    model.getListeCircuit( function (err, result) {
        if (err) {
            // gestion de l'erreur
            console.log(err);
            return;
        }
        response.listeCircuit = result; //On met nos résultats dans liseCircuit
        response.render('listerCircuit', response); //On renvoie notre réponse sur la vue listerCircuit
   }) ;
}

//Affichage de détails d'un circuit
module.exports.DetailCircuit = function(request, response){
    response.title = 'Détail du circuit';
    let numCircuit = request.params.numCircuit; //On récupère le numéro du circuit passé en paramètre dans l'url

    async.parallel([
      function (callback){
        model.getListeCircuit( function (err, result){callback(null,result)});  //Permet d'afficher la vue partielle et de revenir sur un autre circuit
      },
        function (callback){
          model.getDetailCircuitByNumber( numCircuit, (function (err, result){callback(null,result)})); //On récupère les détails du circuit à partir de son numéro
        },
    ],
      function (err, result){
        if (err) {
          console.log(err);
          return;
        }
        response.listeCircuit = result[0];  //On récupère dans un premier temps la liste des circuits
        response.detailCircuit = result[1][0];  //On récupère les détails du circuit
        response.render('detailCircuit', response); //On renvoie notre réponse sur la vue detailCircuit
      }
    );

}
