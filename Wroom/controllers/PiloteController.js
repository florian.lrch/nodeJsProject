//Controler des pilotes
let async = require('async'); //Permet l'éxécution des fonctions de manière asynchrones
let model = require('../models/pilote.js'); //On fait appel au model pilote.js (contient toutes les requêtes SQL)

//On récupère la liste des premières lettre des pilotes répertorié en base de données pour l'afficher dans la vue partielle
module.exports.Repertoire = 	function(request, response){
   response.title = 'Répertoire des pilotes';

   model.getFirstLetterPilote( function (err, result) { //On récupère les première lettres des noms des pilotes dans le model
       if (err) {
           // gestion de l'erreur
           console.log(err);
           return;
       }
       response.piloteFirstLetter = result; //On met le résultat de la requête dans piloteFirstLetter
       //console.log(result);
      response.render('listeLettres', response);  //On renvoie notre résultat dans la vue listeLettres
  }) ;

}

//On affiche les photos ainsi que nom et prénom des pilotes dont le nom commence par la lettre passé en paramètre
module.exports.RepertoirePiloteByLetter = 	function(request, response){
  let letter = request.params.letter; //On récuupère la lettre passé dans l'URL
  response.title = 'Répertoire des pilotes en ' + letter;

  async.parallel([
    function (callback){
      model.getFirstLetterPilote( function (err, result){callback(null,result)}); //On récupère les premières lettres des pilotes pour les afficher dans la vue
    },
      function (callback){
        model.getPiloteByLetter( letter, (function (err, result){callback(null,result)}));  //On récupères les photos, nom et prénom des pilotes commençant par la lettre passée en paramètre
      },
  ],
    function (err, result){
      if (err) {
        console.log(err);
        return;
      }
      response.piloteFirstLetter = result[0];
      response.piloteByLetter = result[1];
      response.render('repertoirePilotes', response); //On renvoie le résultat dans la vue repertoirePilotes
    }
  );
};

//Affichage des informations complètes sur un pilote
module.exports.DescriptionPilote = function(request, response){
  response.title = 'Description du pilote';
  let numeroPilote = request.params.numeroPilote; //On récupère le numéro du pilote passé dans l'URL

  async.parallel([
    function (callback){
      model.getFirstLetterPilote( function (err, result){callback(null,result)}); //On récupère les premières lettres des pilotes pour la vue partielle
    },
    function (callback){
      model.getDescriptionPiloteById( numeroPilote, (function (err, result){callback(null,result)})); //On récupère les principales informations du pilote à partir de son numéro
    },
    function (callback){
      model.getPhotoNonOfficielPiloteById( numeroPilote, (function (err, result){callback(null,result)}));  //On récupère les photos non-officielles du pilotes par son numéro
    },
    function (callback){
      model.getSponsorPiloteById( numeroPilote, (function (err, result){callback(null,result)})); //On récupère les sponsors du pilote par son numéro
    },
    function (callback){
      model.getEcuriePiloteById( numeroPilote, (function (err, result){callback(null,result)}));  //On récupère l'écurie du pilote par son numéro
    },
  ],
    function (err, result){
      if (err) {
        console.log(err);
        return;
      }
      response.piloteFirstLetter = result[0];
      response.descriptionPiloteById = result[1][0];
      response.getPhotoNonOfficielPiloteById = result[2];
      response.sponsorPiloteById = result[3];
      response.getEcuriePiloteById = result[4][0];
      response.render('descriptionPilote', response); //On renvoie les résultat sur la vue descriptionPilote
    }
  );
};
