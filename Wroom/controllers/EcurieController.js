//Controleur des ecuries
let async = require('async'); //Permet l'éxécution des fonctions de manière asynchrones
let model = require('../models/ecurie.js'); //On fait appel au model ecurie.js (contient toutes les requêtes SQL)

//Affichage de la liste des écuries dans la vue partielle
module.exports.ListerEcurie = function(request, response){
  response.title = 'Liste des écuries';
  model.getListeEcurie( function (err, result) {  //On appelle le model pour récupérer la liste des écuries
    if (err) {
      // gestion de l'erreur
      console.log(err);
      return;
    }
    response.listeEcurie = result;  //On met le résultat de la requête dans listeEcurie

    response.render('listerEcurie', response);  //On renvoie notre réponse sur la vue listerEcurie
  });
}

//Affichage du détail des écuries
module.exports.DetailEcurie = function(request, response){
  response.title = 'Détails de l\'écurie';
  let numEcurie = request.params.numEcurie;


  async.parallel([
    function (callback){
      model.getListeEcurie( function (err, result){callback(null,result)}); //On récupère la liste des écuries pour la vue partielle
    },
    function (callback){
      model.getInfoEcurieById( numEcurie, (function (err, result){callback(null,result)})); //On récupère les informations d'une écurie par son numéro
    },
    function (callback){
      model.getPiloteDuneEcurie( numEcurie, (function (err, result){callback(null,result)})); //On récupère les pilotes d'une écurie par son numéro
    },
    function (callback){
      model.getVoitureDuneEcurie( numEcurie, (function (err, result){callback(null,result)}));  //On récupère les voitures d'une écurie par son numéro
    },
    function (callback){
      model.getFournisseurPneumatiqueDuneEcurie( numEcurie, (function (err, result){callback(null,result)})); //On récupère les fournisseur de pneumatique d'une écurie par son numéro
    },
  ],
  function (err, result){
    if (err) {
      console.log(err);
      return;
    }
    //Préparation des infobulles des pilotes : on récupère leur attribut 'piltexte' (contient la description du pilote)

    for (var i = 0; i < result[2].length; i++) {
      result[2][i]['piltexte'] = result[2][i]['piltexte'].substring(0, 115) + "...";  //récupère ensuite les 115 premiers caractères pour pouvoir les afficher dans l'infobulle à l'aide de la fonction js substring
    }

    response.listeEcurie = result[0];
    response.detailEcurie = result[1][0];
    response.piloteEcurie = result[2];
    response.voitureEcurie = result[3];
    response.pneumatiqueEcurie = result[4][0];
    response.render('detailEcurie', response);  //On renvoie notre réponse dans la vue detailEcurie
  }
);
}
