//Fonction permettant l'affichage des photos à taille réelle
$(function () {
    //Pour chaque éléments ayant la classe 'petite' qui sont cliqués
    $(".petite").click(function () {
        let SourcePetiteImage = $(this).attr('src');  //On récupère la source de l'image
        let sujetImage = $(this).attr('title'); //On récupère son sujet
        let commentaireImage = $(this).attr('alt'); //On récupère son commentaire
        let SourceGrandeImage = SourcePetiteImage.replace("petites", "grandes");  //Sur nos élément on remplace la classe petite par grande (voire /public/css/lightbox.css )
        $(".grande").html("<img src='" + SourceGrandeImage + "'><span class='text'>" + sujetImage + ", " + commentaireImage + "</span>"); //On affiche crée notre nouvel élément
        $(".grande").fadeIn("slow").css("display", "flex"); //On l'affiche
    });

    //Pour nos élements en taille réel, on les retire quand ils sont cliqués
    $(".grande").click(function () {
        $(".grande").fadeOut("fast");
    });

});
