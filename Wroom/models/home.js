let db = require('../configDb');

module.exports.getDernierResult = function (callback) {
   // connection à la base
	db.getConnection(function(err, connexion){
        if(!err){
        	  // s'il n'y a pas d'erreur de connexion
        	  // execution de la requête SQL
						let sql ="SELECT g.GPNUM, g.GPNOM, g.GPDATE, g.GPDATEMAJ FROM grandprix g ORDER BY GPDATEMAJ DESC";
            connexion.query(sql, callback);

            // la connexion retourne dans le pool
            connexion.release();
         }
      });
};
