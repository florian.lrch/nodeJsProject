/*
* config.Db contient les parametres de connection à la base de données
* il va créer aussi un pool de connexions utilisables
* sa méthode getConnection permet de se connecter à MySQL
*
*/
let db = require('../configDb');

/* Récupère les première lettre de tous les pilotes présents en base de données */
module.exports.getFirstLetterPilote = function (callback) {
   // connection à la base
	db.getConnection(function(err, connexion){
        if(!err){
        	  // s'il n'y a pas d'erreur de connexion
        	  // execution de la requête SQL
						let sql ="SELECT DISTINCT SUBSTRING(pilnom, 1,1) as piloteFirstLetter FROM pilote order by pilnom ASC"
            connexion.query(sql, callback);

            // la connexion retourne dans le pool
            connexion.release();
         }
      });
};

/* Récupère la liste des pilotes dont leur prénom commence par la lettre passée en paramètre */
module.exports.getPiloteByLetter = function (letter, callback) {
   // connection à la base
	db.getConnection(function(err, connexion){
        if(!err){
        	  // s'il n'y a pas d'erreur de connexion
        	  // execution de la requête SQL
						let sql = "SELECT p.pilnum, p.pilnom, p.pilprenom, ph.phoadresse FROM pilote p JOIN photo ph ON p.pilnum = ph.pilnum WHERE pilnom LIKE '" + letter + "%' AND ph.phonum = 1 ORDER BY pilnom ASC";
            connexion.query(sql, callback);

            // la connexion retourne dans le pool
            connexion.release();
         }
      });
};

/* Récupère les principales informations d'un pilote à partir de son identifiant */
module.exports.getDescriptionPiloteById = function (numeroPilote, callback) {
   // connection à la base
	db.getConnection(function(err, connexion){
        if(!err){
        	  // s'il n'y a pas d'erreur de connexion
        	  // execution de la requête SQL
						let sql = "SELECT p.pilnom, p.pilprenom, p.pildatenais, p.pilpoids, p.piltaille, p.piltexte, pa.paynom, ph.phoadresse, ph.phocommentaire FROM pilote p JOIN pays pa ON p.PAYNUM = pa.PAYNUM JOIN photo ph ON ph.pilnum = p.pilnum WHERE p.pilnum =" + numeroPilote + " AND ph.PHONUM = 1";
            connexion.query(sql, callback);

            // la connexion retourne dans le pool
            connexion.release();
         }
      });
};

module.exports.getEcuriePiloteById = function (numeroPilote, callback) {
   // connection à la base
	db.getConnection(function(err, connexion){
        if(!err){
        	  // s'il n'y a pas d'erreur de connexion
        	  // execution de la requête SQL
						let sql = "SELECT e.ecunom FROM ecurie e JOIN pilote p ON p.ecunum = e.ecunum WHERE p.pilnum = " + numeroPilote;
            connexion.query(sql, callback);

            // la connexion retourne dans le pool
            connexion.release();
         }
      });
};

/* Récupère les noms de toutes les photos d'un pilote à partir de son identifiant */
module.exports.getPhotoNonOfficielPiloteById = function (numeroPilote, callback) {
   // connection à la base
	db.getConnection(function(err, connexion){
        if(!err){
        	  // s'il n'y a pas d'erreur de connexion
        	  // execution de la requête SQL
						let sql = "SELECT ph.phosujet, ph.phocommentaire, ph.phoadresse FROM photo ph WHERE pilnum =" + numeroPilote + " AND phonum != 1";
            connexion.query(sql, callback);

            // la connexion retourne dans le pool
            connexion.release();
         }
      });
};

module.exports.getSponsorPiloteById = function (numeroPilote, callback) {
   // connection à la base
	db.getConnection(function(err, connexion){
        if(!err){
        	  // s'il n'y a pas d'erreur de connexion
        	  // execution de la requête SQL
						let sql = "SELECT sp.sponom, sp.sposectactivite FROM sponsor sp JOIN sponsorise spo ON sp.sponum = spo.sponum WHERE pilnum =" + numeroPilote;
            connexion.query(sql, callback);

            // la connexion retourne dans le pool
            connexion.release();
         }
      });
};
