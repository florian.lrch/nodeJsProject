
let db = require('../configDb');

module.exports.getListeGrandPrix = function (callback) {
   // connection à la base
	db.getConnection(function(err, connexion){
        if(!err){
        	  // s'il n'y a pas d'erreur de connexion
        	  // execution de la requête SQL
						let sql = "SELECT gp.gpnum, gp.gpnom, p.payadrdrap FROM grandprix gp JOIN circuit c ON gp.CIRNUM = c.CIRNUM JOIN pays p ON c.PAYNUM = p.PAYNUM";
						//console.log (sql);
            connexion.query(sql, callback);

            // la connexion retourne dans le pool
            connexion.release();
         }
      });
};

module.exports.getDetailGrandPrixById = function (numGrandPrix, callback) {
   // connection à la base
	db.getConnection(function(err, connexion){
        if(!err){
        	  // s'il n'y a pas d'erreur de connexion
        	  // execution de la requête SQL
						let sql = "SELECT gp.gpnom, gp.gpdate, gp.gpcommentaire FROM grandprix gp WHERE gp.GPNUM = " + numGrandPrix;
						//console.log (sql);
            connexion.query(sql, callback);

            // la connexion retourne dans le pool
            connexion.release();
         }
      });
};

module.exports.getTempsPilote = function (numGrandPrix, callback) {
   // connection à la base
	db.getConnection(function(err, connexion){
        if(!err){
        	  // s'il n'y a pas d'erreur de connexion
        	  // execution de la requête SQL
						let sql = "SELECT p.PILNOM, c.TEMPSCOURSE FROM grandprix gp JOIN course c ON gp.GPNUM = c.GPNUM JOIN pilote p ON c.PILNUM = p.PILNUM WHERE gp.GPNUM = " + numGrandPrix + " ORDER BY c.TEMPSCOURSE ASC";
            connexion.query(sql, callback);

            // la connexion retourne dans le pool
            connexion.release();
         }
      });
};

module.exports.getPlaceNbPoint = function (callback) {
   // connection à la base
	db.getConnection(function(err, connexion){
        if(!err){
        	  // s'il n'y a pas d'erreur de connexion
        	  // execution de la requête SQL
						let sql = "SELECT p.ptplace, p.ptnbpointsplace FROM points p";
						//console.log (sql);
            connexion.query(sql, callback);

            // la connexion retourne dans le pool
            connexion.release();
         }
      });
};
