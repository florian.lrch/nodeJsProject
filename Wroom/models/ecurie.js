/*
* config.Db contient les parametres de connection à la base de données
* il va créer aussi un pool de connexions utilisables
* sa méthode getConnection permet de se connecter à MySQL
*
*/

let db = require('../configDb');

/*
* Récupérer l'intégralité les écuries avec l'adresse de la photo du pays de l'écurie
* @return Un tableau qui contient le N°, le nom de l'écurie et le nom de la photo du drapeau du pays
*/
module.exports.getListeEcurie = function (callback) {
   // connection à la base
	db.getConnection(function(err, connexion){
        if(!err){
        	  // s'il n'y a pas d'erreur de connexion
        	  // execution de la requête SQL
						let sql ="SELECT ecunum, payadrdrap, ecunom FROM " +
                            "ecurie e INNER JOIN pays p ";
						sql= sql + "ON p.paynum=e.paynum ORDER BY ecunom";
            connexion.query(sql, callback);

            // la connexion retourne dans le pool
            connexion.release();
         }
      });
};

module.exports.getInfoEcurieById = function (numEcurie, callback) {
   // connection à la base
	db.getConnection(function(err, connexion){
        if(!err){
        	  // s'il n'y a pas d'erreur de connexion
        	  // execution de la requête SQL
						let sql = "SELECT e.ecunom, e.ecunomdir, e.ecuadrsiege, p.paynom, e.ecuadresseimage FROM ecurie e JOIN pays p ON e.paynum = p.paynum WHERE e.ecunum = " + numEcurie;
						//console.log (sql);
            connexion.query(sql, callback);

            // la connexion retourne dans le pool
            connexion.release();
         }
      });
};

module.exports.getPiloteDuneEcurie = function (numEcurie, callback) {
   // connection à la base
	db.getConnection(function(err, connexion){
        if(!err){
        	  // s'il n'y a pas d'erreur de connexion
        	  // execution de la requête SQL
						let sql = "SELECT p.pilnum, p.pilnom, p.pilprenom, p.piltexte, ph.phoadresse FROM pilote p JOIN ecurie e ON p.ecunum = e.ecunum JOIN photo ph ON p.pilnum = ph.pilnum WHERE ph.phonum = 1 AND e.ecunum = " + numEcurie;
						//console.log (sql);
            connexion.query(sql, callback);

            // la connexion retourne dans le pool
            connexion.release();
         }
      });
};

module.exports.getVoitureDuneEcurie = function (numEcurie, callback) {
   // connection à la base
	db.getConnection(function(err, connexion){
        if(!err){
        	  // s'il n'y a pas d'erreur de connexion
        	  // execution de la requête SQL
						let sql = "SELECT v.voinom, v.voiadresseimage, t.typelibelle FROM voiture v JOIN ecurie e ON v.ecunum = e.ecunum JOIN type_voiture t ON v.typnum = t.typnum WHERE e.ecunum = " + numEcurie;
						//console.log (sql);
            connexion.query(sql, callback);

            // la connexion retourne dans le pool
            connexion.release();
         }
      });
};

module.exports.getFournisseurPneumatiqueDuneEcurie = function (numEcurie, callback) {
   // connection à la base
	db.getConnection(function(err, connexion){
        if(!err){
        	  // s'il n'y a pas d'erreur de connexion
        	  // execution de la requête SQL
						let sql = "SELECT fp.fpnom FROM fourn_pneu fp JOIN ecurie e ON fp.fpnum = e.fpnum WHERE e.ecunum = " + numEcurie;
						//console.log (sql);
            connexion.query(sql, callback);

            // la connexion retourne dans le pool
            connexion.release();
         }
      });
};
