let db = require('../configDb');

//On récupère la liste des différents circuits
module.exports.getListeCircuit = function (callback) {
   // connection à la base
	db.getConnection(function(err, connexion){
        if(!err){
        	  // s'il n'y a pas d'erreur de connexion
        	  // execution de la requête SQL
						let sql = "SELECT c.cirnum, c.cirnom, p.payadrdrap FROM circuit c JOIN pays p ON c.paynum = p.paynum ORDER BY c.cirnom"
            connexion.query(sql, callback);

            // la connexion retourne dans le pool
            connexion.release();
         }
      });
};


module.exports.getDetailCircuitByNumber = function (cirNum, callback) {
   // connection à la base
	db.getConnection(function(err, connexion){
        if(!err){
        	  // s'il n'y a pas d'erreur de connexion
        	  // execution de la requête SQL
						let sql = "SELECT c.cirnom, c.cirlongueur, c.cirnbspectateurs, p.paynom, c.cirtext, c.ciradresseimage FROM circuit c JOIN pays p ON c.paynum = p.paynum WHERE c.cirnum = " + cirNum;
            connexion.query(sql, callback);

            // la connexion retourne dans le pool
            connexion.release();
         }
      });
};
