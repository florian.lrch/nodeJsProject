//On appelle nos différents controleurs
let HomeController = require('./../controllers/HomeController');
let ResultatController = require('./../controllers/ResultatController');
let EcurieController = require('./../controllers/EcurieController');
let PiloteController = require('./../controllers/PiloteController');
let CircuitController = require('./../controllers/CircuitController');

// Routes
module.exports = function(app){

// Main Routes => Acceuil
    app.get('/', HomeController.Index);
    app.get('/accueil', HomeController.Index);

// Pilotes
    app.get('/repertoirePilote', PiloteController.Repertoire);  //Lettre partial

    app.get('/repertoirePilote/:letter', PiloteController.RepertoirePiloteByLetter);  //Liste pilote à partir de la première lettre du nom

    app.get('/descriptionPilote/:numeroPilote', PiloteController.DescriptionPilote);  //Détails d'un pilote à partir de son numéro

 // Circuits
   app.get('/circuits', CircuitController.ListerCircuit); //Liste circuit partial

   app.get('/detailCircuit/:numCircuit', CircuitController.DetailCircuit);  //Détails d'un circuit à partir de son numéro

// Ecuries
   app.get('/ecuries', EcurieController.ListerEcurie);  //Liste écurie partial

   app.get('/detailEcurie/:numEcurie', EcurieController.DetailEcurie);  //Détails d'une écurie à partir de son numéro

 //Résultats
   app.get('/resultats', ResultatController.ListerResultat);  //Liste des résultats

   app.get('/detailGrandPrix/:numGrandPrix', ResultatController.DetailGrandPrix); //Affichage des détails des résultat à partir d'un numéro d'un grand prix


// tout le reste
app.get('*', HomeController.NotFound);
app.post('*', HomeController.NotFound);

};
