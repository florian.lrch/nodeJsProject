let db = require('../configDb');

module.exports.getListePilote = function (callback) {
   // connection à la base
	db.getConnection(function(err, connexion){
        if(!err){
        	  // s'il n'y a pas d'erreur de connexion
        	  // execution de la requête SQL
						let sql ="SELECT p.PILNUM, p.PILNOM, p. PILPRENOM, p.PILDATENAIS FROM pilote p ORDER BY p.PILNOM"
						console.log (sql);
            connexion.query(sql, callback);

            // la connexion retourne dans le pool
            connexion.release();
         }
      });
};

module.exports.getListeNationalite = function (callback) {
   // connection à la base
	db.getConnection(function(err, connexion){
        if(!err){
        	  // s'il n'y a pas d'erreur de connexion
        	  // execution de la requête SQL
						let sql ="SELECT p.PAYNUM, p.PAYNAT FROM pays p"
						console.log (sql);
            connexion.query(sql, callback);

            // la connexion retourne dans le pool
            connexion.release();
         }
      });
};

module.exports.getListeEcurie = function (callback) {
   // connection à la base
	db.getConnection(function(err, connexion){
        if(!err){
        	  // s'il n'y a pas d'erreur de connexion
        	  // execution de la requête SQL
						let sql ="SELECT e.ECUNUM, e.ECUNOM FROM ecurie e"
						console.log (sql);
            connexion.query(sql, callback);

            // la connexion retourne dans le pool
            connexion.release();
         }
      });
};

module.exports.insertPilote = function (prenom, nom, dateNaissance, nat, ecu, points, poids, taille, description, callback) {
   // connection à la base
 	let descriptionCorrige = description.replace(/'/g, "''"); //Permet d'éviter les erreur d'apostrophes
	db.getConnection(function(err, connexion){
        if(!err){
						let sql ="INSERT INTO PILOTE (PILPRENOM, PILNOM, PILDATENAIS, PAYNUM, ECUNUM, PILPOINTS, PILPOIDS, PILTAILLE, PILTEXTE ) VALUES ('" + prenom + "','" + nom + "','" + dateNaissance + "','" + nat + "'," + ecu + ",'" + points + "','" + poids + "','" + taille + "','" + descriptionCorrige + "')"
						console.log (sql);
            connexion.query(sql, callback);

            // la connexion retourne dans le pool
            connexion.release();
         }
      });
};

module.exports.suppPhotoPil = function (pilNum, callback) {
   // connection à la base
	db.getConnection(function(err, connexion){
        if(!err){
						let sql ="DELETE FROM photo WHERE PILNUM = " + pilNum;
						console.log (sql);
            connexion.query(sql, callback);
            // la connexion retourne dans le pool
            connexion.release();
         }
      });
};

module.exports.suppSponsoPil = function (pilNum, callback) {
   // connection à la base
	db.getConnection(function(err, connexion){
        if(!err){
						let sql ="DELETE FROM sponsorise WHERE PILNUM = " + pilNum;
						console.log (sql);
            connexion.query(sql, callback);
            // la connexion retourne dans le pool
            connexion.release();
         }
      });
};

module.exports.suppEssaisPil = function (pilNum, callback) {
   // connection à la base
	db.getConnection(function(err, connexion){
        if(!err){
						let sql ="DELETE FROM essais WHERE PILNUM = " + pilNum;
						console.log (sql);
            connexion.query(sql, callback);
            // la connexion retourne dans le pool
            connexion.release();
         }
      });
};

module.exports.suppCoursePil = function (pilNum, callback) {
   // connection à la base
	db.getConnection(function(err, connexion){
        if(!err){
						let sql ="DELETE FROM course WHERE PILNUM = " + pilNum;
						console.log (sql);
            connexion.query(sql, callback);
            // la connexion retourne dans le pool
            connexion.release();
         }
      });
};

module.exports.suppPil = function (pilNum, callback) {
   // connection à la base
	db.getConnection(function(err, connexion){
        if(!err){
						let sql ="DELETE FROM pilote WHERE PILNUM = " + pilNum;
						console.log (sql);
            connexion.query(sql, callback);
            // la connexion retourne dans le pool
            connexion.release();
         }
      });
};

module.exports.getListePiloteInfos = function (pilNum, callback) {
   // connection à la base
	db.getConnection(function(err, connexion){
        if(!err){
						let sql ="SELECT p.pilnum, p.pilnom, p.pilprenom, p.pildatenais, p.pilpoids, p.piltaille, p.piltexte, pa.paynum, pa.paynat, p.ecunum, p.pilpoints FROM pilote p JOIN pays pa ON p.PAYNUM = pa.PAYNUM WHERE p.pilnum = " + pilNum;
						console.log (sql);
            connexion.query(sql, callback);
            // la connexion retourne dans le pool
            connexion.release();
         }
      });
};
module.exports.getEcuriePilote = function (pilNum, callback) {
   // connection à la base
	db.getConnection(function(err, connexion){
        if(!err){
						let sql ="SELECT e.ecunom, e.ecunum FROM ecurie e JOIN pilote p ON p.ecunum = e.ecunum WHERE p.pilnum =" + pilNum;
						console.log (sql);
            connexion.query(sql, callback);
            // la connexion retourne dans le pool
            connexion.release();
         }
      });
};

module.exports.modifPilote = function (pilnum, prenom, nom, dateNaissance, nat, ecu, points, poids, taille, description, callback) {
 	let descriptionCorrige = description.replace(/'/g, "''");	//Permet d'éviter les erreur d'apostrophes
	 // connection à la base

		db.getConnection(function(err, connexion){
					if(!err){
							let sql ="UPDATE pilote SET PILPRENOM = '" + prenom + "', PILNOM = '" + nom + "', PILDATENAIS = '" + dateNaissance + "', PAYNUM = '" + nat + "', ECUNUM = " + ecu + ", PILPOINTS = '" + points + "', PILPOIDS = '" + poids + "', PILTAILLE = '" + taille + "', PILTEXTE = '" + descriptionCorrige + "' WHERE PILNUM = " + pilnum;
							console.log (sql);
							connexion.query(sql, callback);

							// la connexion retourne dans le pool
							connexion.release();
					 }
				});


};
