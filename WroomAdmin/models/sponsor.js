let db = require('../configDb');

module.exports.getListeSponsor = function (callback) {
   // connection à la base
	db.getConnection(function(err, connexion){
        if(!err){
        	  // s'il n'y a pas d'erreur de connexion
        	  // execution de la requête SQL
						let sql ="SELECT s.SPONUM, s.SPONOM, s.SPOSECTACTIVITE FROM sponsor s ORDER BY s.SPONOM"
						console.log (sql);
            connexion.query(sql, callback);

            // la connexion retourne dans le pool
            connexion.release();
         }
      });
};

module.exports.getListeEcuries = function (callback) {
   // connection à la base
	db.getConnection(function(err, connexion){
        if(!err){
        	  // s'il n'y a pas d'erreur de connexion
        	  // execution de la requête SQL
						let sql ="SELECT e.ECUNUM, e.ECUNOM FROM ecurie e ORDER BY e.ECUNOM";
						console.log (sql);
            connexion.query(sql, callback);

            // la connexion retourne dans le pool
            connexion.release();
         }
      });
};


module.exports.insertSponsor = function (nom, sec_acti, ecu, callback) {
   // connection à la base
	db.getConnection(function(err, connexion){
        if(!err){
						let sql ="INSERT INTO SPONSOR (SPONOM, SPOSECTACTIVITE) VALUES ('" + nom + "','" + sec_acti + "')";
            connexion.query(sql, callback);
						if(ecu != 'null'){
							let sql2 ="SELECT MAX(SPONUM) AS MAXSPONSOR FROM sponsor";
							connexion.query(sql2, function (err, result, fields) {
								if (err) throw err;
								let last_sponsor_num = result[0]['MAXSPONSOR'];
								let sql3 = "INSERT INTO FINANCE (ECUNUM, SPONUM) VALUES ('" + ecu + "','" + last_sponsor_num + "')";
								connexion.query(sql3, callback);
							});
						}
            // la connexion retourne dans le pool
            connexion.release();
         }
      });
};

module.exports.suppSponsorise = function (spoNum, callback) {
   // connection à la base
	db.getConnection(function(err, connexion){
        if(!err){
        	  // s'il n'y a pas d'erreur de connexion
        	  // execution de la requête SQL
						let sql ="DELETE FROM sponsorise WHERE SPONUM = " + spoNum;
						console.log (sql);
            connexion.query(sql, callback);

            // la connexion retourne dans le pool
            connexion.release();
         }
      });
};

module.exports.suppFinance = function (spoNum, callback) {
   // connection à la base
	db.getConnection(function(err, connexion){
        if(!err){
        	  // s'il n'y a pas d'erreur de connexion
        	  // execution de la requête SQL
						let sql ="DELETE FROM finance WHERE SPONUM = " + spoNum;
						console.log (sql);
            connexion.query(sql, callback);

            // la connexion retourne dans le pool
            connexion.release();
         }
      });
};

module.exports.suppSponsor = function (spoNum, callback) {
   // connection à la base
	db.getConnection(function(err, connexion){
        if(!err){
        	  // s'il n'y a pas d'erreur de connexion
        	  // execution de la requête SQL
						let sql ="DELETE FROM sponsor WHERE SPONUM = " + spoNum;
						console.log (sql);
            connexion.query(sql, callback);

            // la connexion retourne dans le pool
            connexion.release();
         }
      });
};
module.exports.getListeSponsorInfos = function (spoNum, callback) {
   // connection à la base
	db.getConnection(function(err, connexion){
        if(!err){
        	  // s'il n'y a pas d'erreur de connexion
        	  // execution de la requête SQL
						let sql ="SELECT s.SPONUM, s.SPONOM, s.SPOSECTACTIVITE FROM sponsor s WHERE s.SPONUM = " + spoNum;
						console.log(sql);
						connexion.query(sql, callback);
            // la connexion retourne dans le pool
            connexion.release();
         }
      });
};

module.exports.getListeSponsorEcuNom = function (spoNum, callback) {
   // connection à la base
	db.getConnection(function(err, connexion){
        if(!err){
        	  // s'il n'y a pas d'erreur de connexion
        	  // execution de la requête SQL
						let sql ="SELECT f.ECUNUM as ECUNUM FROM finance f WHERE f.SPONUM = " + spoNum;
						console.log(sql);
						connexion.query(sql, function (err, result, fields) {
							if (err) throw err;
							if(result.length == 0){
								let sql2 = "SELECT * FROM points";
								connexion.query(sql2, callback);
							} else {
								let ecunum = result[0]['ECUNUM'];
								let sql2 = "SELECT ECUNUM, ECUNOM FROM ecurie WHERE ECUNUM = " + ecunum;
								console.log(sql2);
								connexion.query(sql2, callback);
							}
						});
            // la connexion retourne dans le pool
						connexion.release();

         }
      });
};

module.exports.updateSponsor = function (sponum, nom, sec_acti, ecu, callback) {
   // connection à la base
	db.getConnection(function(err, connexion){
        if(!err){
						let sql ="UPDATE sponsor SET SPONOM = '" + nom + "', SPOSECTACTIVITE = '" + sec_acti + "' WHERE SPONUM = " + sponum;
						console.log(sql);
            connexion.query(sql, callback);
						//On vérifie sur l'écurie est supprimé ou non
						if (ecu == 'null') {
							let sql2 = "DELETE FROM finance WHERE sponum = " + sponum;
							console.log(sql2);
							connexion.query(sql2, callback);
						} else {
							let sql3 = "UPDATE finance SET ECUNUM = '" + ecu + "' WHERE SPONUM = " + sponum;
							console.log(sql3);
							connexion.query(sql3, callback);
							let sql4 = "INSERT INTO FINANCE (ECUNUM, SPONUM) VALUES ('" + ecu + "','" + sponum + "')";
							console.log(sql4);
							connexion.query(sql4, callback);
						}

            // la connexion retourne dans le pool
            connexion.release();
         }
      });
};
