let db = require('../configDb');

module.exports.getListeCircuit = function (callback) {
   // connection à la base
	db.getConnection(function(err, connexion){
        if(!err){
        	  // s'il n'y a pas d'erreur de connexion
        	  // execution de la requête SQL
						let sql ="SELECT c.CIRNUM, c.CIRNOM, c.CIRLONGUEUR, c.CIRNBSPECTATEURS, gp.GPNUM FROM circuit c LEFT JOIN grandprix gp ON c.CIRNUM = gp.CIRNUM ORDER BY c.CIRNOM"
						console.log (sql);
            connexion.query(sql, callback);

            // la connexion retourne dans le pool
            connexion.release();
         }
      });
};

module.exports.getListeNationalite = function (callback) {
   // connection à la base
	db.getConnection(function(err, connexion){
        if(!err){
        	  // s'il n'y a pas d'erreur de connexion
        	  // execution de la requête SQL
						let sql ="SELECT p.PAYNUM, p.PAYNOM FROM pays p ORDER BY p.PAYNOM";
						console.log (sql);
            connexion.query(sql, callback);

            // la connexion retourne dans le pool
            connexion.release();
         }
      });
};

module.exports.insertCircuit = function (nom, longueur, nat, img, nbSpectateur, description, callback) {
   // connection à la base
 	let descriptionCorrige = description.replace(/'/g, "''");	//Permet d'éviter les erreur d'apostrophes
	db.getConnection(function(err, connexion){
        if(!err){
						let sql ="INSERT INTO CIRCUIT (CIRNOM, CIRLONGUEUR, PAYNUM, CIRADRESSEIMAGE, CIRNBSPECTATEURS, CIRTEXT ) VALUES ('" + nom + "','" + longueur + "','" + nat + "','" + img + "','" + nbSpectateur + "','" + descriptionCorrige + "')";
						console.log (sql);
            connexion.query(sql, callback);

            // la connexion retourne dans le pool
            connexion.release();
         }
      });
};



module.exports.suppCourseCir = function (gpnum, callback) {
   // connection à la base
	db.getConnection(function(err, connexion){
    if(!err){
			let sql = "DELETE FROM course WHERE GPNUM = " + gpnum;
			console.log(sql);
			connexion.query(sql, callback);
		}
    connexion.release();
  });
};

module.exports.suppEssaisCir = function (gpnum, callback) {
   // connection à la base
	db.getConnection(function(err, connexion){
	  if(!err){
			let sql = "DELETE FROM essais WHERE GPNUM = " + gpnum;
			console.log(sql);
			connexion.query(sql, callback);
		}
    connexion.release();
  });
};


module.exports.suppGrandPrix = function (cirNum, callback) {
   // connection à la base
	db.getConnection(function(err, connexion){
	  if(!err){
			let sql = "DELETE FROM grandprix WHERE CIRNUM = " + cirNum;
			console.log(sql);
			connexion.query(sql, callback);
		}
	  connexion.release();
	});
};

module.exports.suppCircuit = function (cirNum, callback) {
   // connection à la base
	db.getConnection(function(err, connexion){
	  if(!err){
			let sql = "DELETE FROM circuit WHERE CIRNUM = " + cirNum;
			console.log(sql);
			connexion.query(sql, callback);
		}
	  connexion.release();
	});
};

module.exports.getListeCircuitInfos = function (cirNum, callback) {
   // connection à la base
	db.getConnection(function(err, connexion){
        if(!err){
        	  // s'il n'y a pas d'erreur de connexion
        	  // execution de la requête SQL
						let sql = "SELECT c.cirnum, c.cirnom, c.cirlongueur, c.cirnbspectateurs, p.paynom, p.paynum, c.cirtext FROM circuit c JOIN pays p ON c.paynum = p.paynum WHERE c.cirnum = " + cirNum;
						console.log (sql);
            connexion.query(sql, callback);

            // la connexion retourne dans le pool
            connexion.release();
         }
      });
};

module.exports.modifCircuit = function (cirnum, nom, longueur, nat, img, nbSpectateur, description, callback) {
   // connection à la base
	let descriptionCorrige = description.replace(/'/g, "''");		//Permet d'éviter les erreur d'apostrophes
	db.getConnection(function(err, connexion){
        if(!err){
						let sql ="UPDATE circuit SET cirnom = '" + nom + "', cirlongueur = '" + longueur + "', paynum = '" + nat + "', ciradresseimage = '" + img + "', cirnbspectateurs = '" + nbSpectateur + "', cirtext = '" + descriptionCorrige + "' WHERE CIRNUM = " + cirnum;
						console.log (sql);
            connexion.query(sql, callback);

            // la connexion retourne dans le pool
            connexion.release();
         }
      });
};
