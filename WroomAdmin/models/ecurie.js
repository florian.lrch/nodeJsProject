let db = require('../configDb');

module.exports.getListeEcurie = function (callback) {
   // connection à la base
	db.getConnection(function(err, connexion){
        if(!err){
        	  // s'il n'y a pas d'erreur de connexion
        	  // execution de la requête SQL
						let sql ="SELECT e.ECUNUM, e.ECUNOM, e.ECUNOMDIR, e.ECUPOINTS FROM ecurie e ORDER BY e.ECUNOM"
						console.log (sql);
            connexion.query(sql, callback);

            // la connexion retourne dans le pool
            connexion.release();
         }
      });
};

module.exports.getListeNationalite = function (callback) {
   // connection à la base
	db.getConnection(function(err, connexion){
        if(!err){
        	  // s'il n'y a pas d'erreur de connexion
        	  // execution de la requête SQL
						let sql ="SELECT p.PAYNUM, p.PAYNOM FROM pays p ORDER BY p.PAYNOM";
						console.log (sql);
            connexion.query(sql, callback);

            // la connexion retourne dans le pool
            connexion.release();
         }
      });
};

module.exports.insertEcurie = function (nom, directeur, adr_siege, nb_points, nat, imgName, callback) {
   // connection à la base
	db.getConnection(function(err, connexion){
        if(!err){
						let sql ="INSERT INTO ECURIE (ECUNOM, ECUNOMDIR, ECUADRSIEGE, ECUPOINTS, PAYNUM, ECUADRESSEIMAGE ) VALUES ('" + nom + "','" + directeur + "','" + adr_siege + "','" + nb_points + "','" + nat + "','" + imgName + "')";
						console.log (sql);
            connexion.query(sql, callback);

            // la connexion retourne dans le pool
            connexion.release();
         }
      });
};

module.exports.suppFinance = function (ecuNum ,callback) {
   // connection à la base
	db.getConnection(function(err, connexion){
        if(!err){
						let sql ="DELETE FROM finance WHERE ECUNUM = " + ecuNum;
						console.log (sql);
            connexion.query(sql, callback);

            // la connexion retourne dans le pool
            connexion.release();
         }
      });
};

module.exports.modifEcuPilote = function (ecuNum ,callback) {
   // connection à la base
	db.getConnection(function(err, connexion){
        if(!err){
						let sql ="UPDATE pilote SET ECUNUM = NULL WHERE ECUNUM = " + ecuNum;
						console.log (sql);
            connexion.query(sql, callback);

            // la connexion retourne dans le pool
            connexion.release();
         }
      });
};

module.exports.suppEcu = function (ecuNum ,callback) {
   // connection à la base
	db.getConnection(function(err, connexion){
        if(!err){
						let sql ="DELETE FROM ecurie WHERE ECUNUM = " + ecuNum;
						console.log (sql);
            connexion.query(sql, callback);

            // la connexion retourne dans le pool
            connexion.release();
         }
      });
};

module.exports.getListeEcurieInfos = function (numEcurie, callback) {
   // connection à la base
	db.getConnection(function(err, connexion){
        if(!err){
        	  // s'il n'y a pas d'erreur de connexion
        	  // execution de la requête SQL
						let sql = "SELECT e.ecunum, e.ecunom, e.ecunomdir, e.ecuadrsiege, p.paynum, p.paynom, e.ecupoints FROM ecurie e JOIN pays p ON e.paynum = p.paynum WHERE e.ecunum = " + numEcurie;
						//console.log (sql);
            connexion.query(sql, callback);

            // la connexion retourne dans le pool
            connexion.release();
         }
      });
};

module.exports.modifEcurie = function (ecunum, nom, directeur, adr_siege, nb_points, nat, imgName, callback) {
   // connection à la base
	db.getConnection(function(err, connexion){
        if(!err){
						let sql ="UPDATE ecurie SET ecunom = '" + nom + "', ecunomdir = '" + directeur + "', ecuadrsiege = '" + adr_siege + "', ecupoints = '" + nb_points + "', paynum = '" + nat + "', ecuadresseimage = '" + imgName + "' WHERE ecunum = " + ecunum;
						console.log (sql);
            connexion.query(sql, callback);

            // la connexion retourne dans le pool
            connexion.release();
         }
      });
};
