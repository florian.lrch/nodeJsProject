let db = require('../configDb');

module.exports.getListeGrandPrix = function (callback) {
   // connection à la base
	db.getConnection(function(err, connexion){
        if(!err){
        	  // s'il n'y a pas d'erreur de connexion
        	  // execution de la requête SQL
						let sql ="SELECT gpnum, gpnom FROM grandprix"
						console.log (sql);
            connexion.query(sql, callback);

            // la connexion retourne dans le pool
            connexion.release();
         }
      });
};

module.exports.getTempsPilote = function (numGrandPrix, callback) {
   // connection à la base
	db.getConnection(function(err, connexion){
        if(!err){
        	  // s'il n'y a pas d'erreur de connexion
        	  // execution de la requête SQL
						let sql = "SELECT p.PILNUM, p.PILNOM, c.TEMPSCOURSE FROM grandprix gp JOIN course c ON gp.GPNUM = c.GPNUM JOIN pilote p ON c.PILNUM = p.PILNUM WHERE gp.GPNUM = " + numGrandPrix + " ORDER BY c.TEMPSCOURSE ASC";
            connexion.query(sql, callback);

            // la connexion retourne dans le pool
            connexion.release();
         }
      });
};

module.exports.getPlaceNbPoint = function (callback) {
   // connection à la base
	db.getConnection(function(err, connexion){
        if(!err){
        	  // s'il n'y a pas d'erreur de connexion
        	  // execution de la requête SQL
						let sql = "SELECT p.ptplace, p.ptnbpointsplace FROM points p";
						//console.log (sql);
            connexion.query(sql, callback);

            // la connexion retourne dans le pool
            connexion.release();
         }
      });
};

module.exports.getPiloteSansResultat = function (numGrandPrix, callback) {
   // connection à la base
	db.getConnection(function(err, connexion){
        if(!err){
        	  // s'il n'y a pas d'erreur de connexion
        	  // execution de la requête SQL
						let sql = "SELECT p.pilnum, p.pilnom FROM pilote p WHERE p.pilnum	NOT IN (SELECT c.pilnum FROM course c WHERE gpnum = " + numGrandPrix + ") ORDER BY p.pilnom";
						//console.log (sql);
            connexion.query(sql, callback);

            // la connexion retourne dans le pool
            connexion.release();
         }
      });
};

module.exports.ajoutResultat = function (gpnum, pilnum, tempsCouse, callback) {
   // connection à la base
	db.getConnection(function(err, connexion){
        if(!err){
        	  // s'il n'y a pas d'erreur de connexion
        	  // execution de la requête SQL
						let sql = "INSERT INTO course (gpnum, pilnum, tempscourse) VALUE (" + gpnum + "," + pilnum + ",'" + tempsCouse + "')";
						//console.log (sql);
            connexion.query(sql, callback);

            // la connexion retourne dans le pool
            connexion.release();
         }
      });
};


module.exports.numDixPremierPilotes = function (gpnum, callback) {
   // connection à la base
	db.getConnection(function(err, connexion){
        if(!err){
        	  // s'il n'y a pas d'erreur de connexion
        	  // execution de la requête SQL
						let sql = "SELECT c.pilnum, p.pilpoints, e.ecupoints, e.ecunum FROM course c JOIN pilote p ON c.pilnum = p.pilnum LEFT JOIN ecurie e ON p.ecunum = e.ecunum WHERE c.gpnum = " + gpnum + " ORDER BY tempscourse";
						connexion.query(sql, callback)
            console.log(sql);
            connexion.release();
         }
      });
};

module.exports.modifPointsPil = function (pilnum, pointsUpdate, callback) {

   // connection à la base
	db.getConnection(function(err, connexion){
        if(!err){
        	  // s'il n'y a pas d'erreur de connexion
        	  // execution de la requête SQL
						let sql = "UPDATE pilote SET pilpoints = '" + pointsUpdate + "' WHERE pilnum = " + pilnum;
						connexion.query(sql, callback)
            console.log(sql);
            connexion.release();
         }
      });
};

module.exports.modifPointsEcu = function (ecunum, pointsUpdate, callback) {

   // connection à la base
	db.getConnection(function(err, connexion){
        if(!err){
        	  // s'il n'y a pas d'erreur de connexion
        	  // execution de la requête SQL
						let sql = "UPDATE ecurie e SET e.ecupoints = '" + pointsUpdate + "' WHERE ecunum = " + ecunum;
						connexion.query(sql, callback)
            console.log(sql);
            connexion.release();
         }
      });
};

module.exports.getEcuPil = function (pilnum, callback) {

   // connection à la base
	db.getConnection(function(err, connexion){
        if(!err){
        	  // s'il n'y a pas d'erreur de connexion
        	  // execution de la requête SQL
						let sql = "SELECT ecunum FROM pilote WHERE pilnum = " + pilnum;
						connexion.query(sql, callback)
            console.log(sql);
            connexion.release();
         }
      });
};


module.exports.supp = function (gpnum, pilnum, callback) {
   // connection à la base
	db.getConnection(function(err, connexion){
	  if(!err){
	  	  // s'il n'y a pas d'erreur de connexion
	  	  // execution de la requête SQL
				let sql = "DELETE FROM course WHERE pilnum = " + pilnum + " AND gpnum = " + gpnum;
				//console.log (sql);
	      connexion.query(sql, callback);

	      // la connexion retourne dans le pool
	      connexion.release();
	  }
  });
};
