let HomeController = require('./../controllers/HomeController');
let PiloteController = require('./../controllers/PiloteController');
let CircuitController = require('./../controllers/CircuitController');
let EcurieController = require('./../controllers/EcurieController');
let SponsorController = require('./../controllers/SponsorController');
let ConnexionController = require('./../controllers/ConnexionController');
let ResultatController = require('./../controllers/ResultatController');
// Routes
module.exports = function(app){

    app.get('/', PiloteController.ListePilote);

// Routes CONNEXION

    app.post('/connexion', ConnexionController.Connexion);

    app.get('/deconnexion', ConnexionController.Deconnexion);
//Routes PILOTES
    app.get('/pilote', PiloteController.ListePilote);

    app.get('/pilote/ajout', PiloteController.AjoutPilote);

    app.post('/ajoutPiloteReq', PiloteController.AjoutPiloteReq);

    app.get('/suppPilote/:pilNum', PiloteController.SupprimerPilote);

    app.get('/modifPilote/:pilNum', PiloteController.ModifierPilote);

    app.post('/modifPiloteReq/:pilNum', PiloteController.ModifierPiloteReq);

//Routes CIRCUITS
    app.get('/circuit', CircuitController.ListeCircuit);

    app.get('/circuit/ajout', CircuitController.AjoutCircuit);

    app.post('/ajoutCircuitReq', CircuitController.AjoutCircuitReq);

    app.get('/suppCircuit/:cirNum/:gpNum', CircuitController.SupprimerCircuit);

    app.get('/modifierCircuit/:cirNum', CircuitController.ModifierCircuit);

    app.post('/modifierCircuitReq/:cirNum', CircuitController.ModifierCircuitReq);

//Routes ECURIES
    app.get('/ecurie', EcurieController.ListeEcurie);

    app.get('/ecurie/ajout', EcurieController.AjoutEcurie);

    app.post('/ajoutEcurieReq', EcurieController.AjoutEcurieReq);

    app.get('/suppEcurie/:ecuNum', EcurieController.SupprimerEcurie);

    app.get('/modifierEcurie/:ecuNum', EcurieController.ModifierEcurie);

    app.post('/modifierEcurieReq/:ecuNum', EcurieController.ModifierEcurieReq);

//Routes SPONSORS
    app.get('/sponsor', SponsorController.ListeSponsor);

    app.get('/sponsor/ajout', SponsorController.AjoutSponsor);

    app.post('/ajoutSponsorReq', SponsorController.AjoutSponsorReq);

    app.get('/suppSponsor/:spoNum', SponsorController.SupprimerSponsor);

    app.get('/modifierSponsor/:spoNum', SponsorController.ModifierSponsor);

    app.post('/modifierSponsorReq/:spoNum', SponsorController.ModifierSponsorReq);

//Routes RESULTATS
    app.get('/resultat', ResultatController.AfficherGrandPrix);

    app.post('/saisirResultat', ResultatController.SaisirResultat);

    app.get('/saisirResultat/:gpnum', ResultatController.SaisirResultatGp);

    app.post('/ajoutResultat/:gpnum', ResultatController.AjoutResultat);

    app.post('/suppResultat', ResultatController.SuppResultat);

// tout le reste
app.get('*', HomeController.NotFound);
app.post('*', HomeController.NotFound);

};
