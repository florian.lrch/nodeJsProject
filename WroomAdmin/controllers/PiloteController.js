//Répertoire des pilotes
let async = require('async'); //Permet l'éxécution des fonctions de manière asynchrones
let model = require('../models/pilote.js'); //On fait appel au model circuit.js (contient toutes les requêtes SQL)

//On renvoie la liste des pilotes
module.exports.ListePilote = 	function(request, response){
   response.title = 'Répertoire des pilotes';
   //On fait une requête SQL dans le model pour récupérer la liste des pilotes
   model.getListePilote( function (err, result) {
       if (err) {
           // gestion de l'erreur
           console.log(err);
           return;
       }
       response.listePilote = result; //On stock le résultat dans listePilote
       //console.log(result);
      response.render('listePilote', response); //On renvoie le résultat sur la page listePilote
  }) ;

}

//Affichage de la page d'ajout d'un pilote
module.exports.AjoutPilote = 	function(request, response){
   response.title = 'Ajout d\'un pilote';
   //On récupère les nationalités et les écuries pour les afficher dans le formulaire
   async.parallel([
     function (callback){
       model.getListeNationalite( function (err, result){callback(null,result)});
     },
     function (callback){
       model.getListeEcurie( function (err, result){callback(null,result)});
     },
   ],
     function (err, result){
       if (err) {
         console.log(err);
         return;
       }
       //On stock les résultats de nos requêtes
       response.listeNationalite = result[0];
       response.listeEcurie = result[1];

      response.render('listePilote', response); //On le renvoie vers la page listePilote
     }
   );
}

//Ajout d'un pilote en base de données
module.exports.AjoutPiloteReq = 	function(request, response){
  response.title = 'Ajout pilote';

  //On récupère toutes nos informations passé dans le formulaire
  let prenom = request.body.prenom;
  let nom = request.body.nom;
  let dateNaissance = request.body.dateNaissance;
  let nat = request.body.nat;
  let ecu = request.body.ecu;
  let points = request.body.points;
  let poids = request.body.poids;
  let taille = request.body.taille;
  let description = request.body.description;

  //On les insert en base de données
  model.insertPilote(prenom, nom, dateNaissance, nat, ecu, points, poids, taille, description, function (err, result) {
      if (err) {
          // gestion de l'erreur
          console.log(err);
          return;
      }
      //console.log(result);
      response.writeHead(301,{Location: '/pilote'});  //On renvoie sur la page des pilotes
      response.end();
 }) ;

}

//Suppression d'un pilote
module.exports.SupprimerPilote = 	function(request, response){
  response.title = 'Répertoire des pilotes';
  let pilNum = request.params.pilNum; //On récupère le numéro du pilote que l'on souhaite supprimer

  //On effectue la suppression du pilote et de toutes les éléments qui lui sont associés
  async.parallel([
    function (callback){
      model.suppPhotoPil( pilNum, function (err, result){callback(null,result)});
    },
    function (callback){
      model.suppSponsoPil( pilNum, (function (err, result){callback(null,result)}));
    },
    function (callback){
      model.suppEssaisPil( pilNum, (function (err, result){callback(null,result)}));
    },
    function (callback){
      model.suppCoursePil( pilNum, (function (err, result){callback(null,result)}));
    },
    function (callback){
      model.suppPil( pilNum, (function (err, result){callback(null,result)}));
    },
  ],
    function (err, result){
      if (err) {
        console.log(err);
        return;
      }

      response.writeHead(301,{Location: '/pilote'});  //On renvoie vers la page des pilotes
      response.end();
    }
  );
};

//Page de modification d'un pilote
module.exports.ModifierPilote = 	function(request, response){
  response.title = 'Modification du pilote';
  let pilNum = request.params.pilNum; //On récupère le numéro du pilote que l'on souhaite modifier

  //On récupère ses informations en base de données pour préremplir les champs
  async.parallel([
    function (callback){
      model.getListePiloteInfos( pilNum, function (err, result){callback(null,result)});
    },
    function (callback){
      model.getListeNationalite( function (err, result){callback(null,result)});
    },
    function (callback){
      model.getListeEcurie( function (err, result){callback(null,result)});
    },
    function (callback){
      model.getEcuriePilote( pilNum, function (err, result){callback(null,result)});
    },
  ],
    function (err, result){
      if (err) {
        console.log(err);
        return;
      }
      response.listePiloteModif = result[0][0];
      response.listeNationaliteModif = result[1];
      response.listeEcurieModif = result[2];
      response.listeEcuPilModif = result[3][0];

      response.render('listePilote', response); //On renvoie vers la listePilote
    }
  );
};

//On modifie le pilote en base de données
module.exports.ModifierPiloteReq = 	function(request, response){
  response.title = 'Répertoire des pilotes';

  //On récupère les données du formulaire
  let pilnum = request.params.pilNum;
  let prenom = request.body.prenom;
  let nom = request.body.nom;
  let dateNaissance = request.body.dateNaissance;
  let nat = request.body.nat;
  let ecu = request.body.ecu;
  let points = request.body.points;
  let poids = request.body.poids;
  let taille = request.body.taille;
  let description = request.body.description;
  console.log(ecu)

  //On éxécute la modification sur la base de données
  model.modifPilote(pilnum, prenom, nom, dateNaissance, nat, ecu, points, poids, taille, description, function (err, result) {
      if (err) {
          // gestion de l'erreur
          console.log(err);
          return;
      }
      //console.log(result);
      response.writeHead(301,{Location: '/pilote'});  //On renvoie sur la page des pilotes
      response.end();
 }) ;

}
