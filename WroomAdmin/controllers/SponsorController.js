//Répertoires des sponsors
let async = require('async'); //Permet l'éxécution des fonctions de manière asynchrones
let model = require('../models/sponsor.js');  //On fait appel au model sponsor.js (contient toutes les requêtes SQL)

//Affichage des sponsor
module.exports.ListeSponsor = 	function(request, response){
   response.title = 'Répertoire des sponsors';

   model.getListeSponsor( function (err, result) {  //Requête des sponsor
       if (err) {
           console.log(err);
           return;
       }
       response.listeSponsor = result;

      response.render('listeSponsor', response);  //Renvoie sur listeSponsor
  }) ;

}


//Affichage de la page d'ajout de sponsors
module.exports.AjoutSponsor = 	function(request, response){
   response.title = 'Ajouter un sponsor';

   //On récupère la liste des écuries
   model.getListeEcuries( function (err, result) {
       if (err) {
           console.log(err);
           return;
       }
       response.listeEcurie = result;

      response.render('listeSponsor', response);  //On renvoie sur listeSponsor
  }) ;
}


//Ajout d'un sponsor en base de données
module.exports.AjoutSponsorReq = 	function(request, response){
   response.title = 'Ajouter un sponsor';
   //On récupère les données du formulaire
   let nom = request.body.nom;
   let sec_acti = request.body.sec_acti;
   let ecu = request.body.ecu;
   //On les insert en base de données
   model.insertSponsor(nom, sec_acti, ecu, function (err, result) {
     if (err) {
     // gestion de l'erreur
     console.log(err);
     return;
     }
     //console.log(result);
    }) ;
    response.writeHead(301,{Location: '/sponsor'}); //On renvoie sur la page des sponsors
    response.end();
}

//Suppression d'un sponsor
module.exports.SupprimerSponsor = 	function(request, response){
   response.title = 'Supprimer un sponsor';
   let spoNum = request.params.spoNum;  //On récupère le numéro du sponsor que l'on souhaite supprimer

   //On supprime le sponsor et les éléments qui lui sont liés
   async.parallel([
     function (callback){
       model.suppSponsorise( spoNum, function (err, result){callback(null,result)});
     },
     function (callback){
       model.suppFinance( spoNum, (function (err, result){callback(null,result)}));
     },
     function (callback){
       model.suppSponsor( spoNum, (function (err, result){callback(null,result)}));
     },
   ],
     function (err, result){
       if (err) {
         console.log(err);
         return;
       }

       response.writeHead(301,{Location: '/sponsor'});  //On renvoie sur la page des sponsors
       response.end();
     }
   );
 };

 //Page de modification d'un sponsor
 module.exports.ModifierSponsor = 	function(request, response){
    response.title = 'Modifier le sponsor';
    let spoNum = request.params.spoNum; //On récupère le numéro du sponsor que l'on souhaite modifier

    //On récupère les informations du sponsor
    async.parallel([
      function (callback){
        model.getListeSponsorInfos( spoNum, function (err, result){callback(null,result)});
      },
      function (callback){
        model.getListeEcuries( (function (err, result){callback(null,result)}));
      },
      function (callback){
        model.getListeSponsorEcuNom( spoNum, (function (err, result){callback(null,result)}));
      },
    ],
      function (err, result){
        if (err) {
          console.log(err);
          return;
        }
        response.listeSponsorModif = result[0][0];
        response.listeEcurieModif = result[1];
        //On vérifie que le sponsor a une écurie
        if(result[2].length == 1){
          response.sponsorEcuNom = result[2][0];  //Si oui alors on passe le résultat de la requête dans sposorEcuNom
        }
        response.render('listeSponsor', response);  //On renvoie sur liste sponsor
      }
    );
  };

  //Modification des informations d'un sponsor en base de données
  module.exports.ModifierSponsorReq = 	function(request, response){
     response.title = 'Répertoire des sponsors';

     //On récupère les données du formulaire
     let sponum = request.params.spoNum;
     let nom = request.body.nom;
     let sec_acti = request.body.sec_acti;
     let ecu = request.body.ecu;

     //On effectue la modification dans la base de données
     model.updateSponsor(sponum, nom, sec_acti, ecu, function (err, result) {
       if (err) {
       // gestion de l'erreur
       console.log(err);
       return;
       }
       //console.log(result);
      }) ;
      response.writeHead(301,{Location: '/sponsor'}); //On renvoie vers la page des sponsors
      response.end();
  }
