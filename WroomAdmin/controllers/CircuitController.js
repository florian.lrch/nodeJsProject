//Controleur des pilotes
let async = require('async');  //Permet l'éxécution des fonctions de manière asynchrones
let model = require('../models/circuit.js'); //On fait appel au model circuit.js (contient toutes les requêtes SQL)

//Affiche la liste des circuits
module.exports.ListeCircuit = 	function(request, response){
   response.title = 'Répertoire des circuits';

   //On fait appel au model pour éxécuter la requête SQL
   model.getListeCircuit( function (err, result) {
       if (err) {
           console.log(err);
           return;
       }
       response.listeCircuit = result;  //On met le résultat dans listeCircuit

      response.render('listeCircuit', response);  //On retourne notre réponse sur listeCircuit
  }) ;
}

//Affiche la page d'ajout d'un circuit
module.exports.AjoutCircuit = 	function(request, response){
   response.title = 'Ajouter un circuit';

   //On récupère la liste des pays afin de l'afficher dans le formulaire
   model.getListeNationalite( function (err, result) {
       if (err) {
           console.log(err);
           return;
       }
       response.listeNationalite = result;  //On met le resultat dans listeNationalite

      response.render('listeCircuit', response);  //On retourne notre réponse sur listenationalite
  }) ;
}

//Récupère les données passées dans le formulaire et on les insert en base de données
module.exports.AjoutCircuitReq = 	function(request, response){
   response.title = 'Répertoire des circuits';

   //Récupération des différents paramètres dans le formulaire
   let nom = request.body.nom;
   let longueur = request.body.longueur;
   let nat = request.body.nat;
   let img = request.files.upload;
   let imgName = request.files.upload.name;
   let nbSpectateur = request.body.nbSpectateur;
   let description = request.body.description;

   //On upload l'image dans le dossier correspondant aux images de circuits (on le fait côté public et admin)
   img.mv('../Wroom/public/image/circuit/' + imgName , function(err) {
    if (err)
      console.log(response.status(500).send(err));

    console.log('File uploaded public')
  });
  img.mv('./public/image/circuit/' + imgName , function(err) {
   if (err)
     console.log(response.status(500).send(err));

   console.log('File uploaded admin')
 });

  //On insert les informations du circuit en base de données
   model.insertCircuit(nom, longueur, nat, imgName, nbSpectateur, description, function (err, result) {
       if (err) {
           // gestion de l'erreur
           console.log(err);
           return;
       }
       //console.log(result);
       response.writeHead(301,{Location: '/circuit'});  //On renvoie sur la page des circuits
       response.end();
   }) ;
}

//Permet la suppression d'un circuit
module.exports.SupprimerCircuit = 	function(request, response){
   response.title = 'Répertoire des circuits';
   //On récupère les informations relative au circuit
   let cirNum = request.params.cirNum;
   let gpNum = request.params.gpNum;

   //On éxécute les différentes requête pour supprimer le circuit ainsi que le grand prix associé
   async.parallel([
     function (callback){
       model.suppCourseCir( gpNum, function (err, result){callback(null,result)});
     },
     function (callback){
       model.suppEssaisCir( gpNum, (function (err, result){callback(null,result)}));
     },
     function (callback){
       model.suppGrandPrix( cirNum, (function (err, result){callback(null,result)}));
     },
     function (callback){
       model.suppCircuit( cirNum, (function (err, result){callback(null,result)}));
     },
   ],
     function (err, result){
       if (err) {
         console.log(err);
         return;
       }
       response.writeHead(301,{Location: '/circuit'});  //Enfin on renvoie sur la page des circuits
       response.end();
     }
   );
 };

 //Permet la modification d'un circuit
 module.exports.ModifierCircuit = 	function(request, response){
    response.title = 'Modification du circuit';
    //On récupère le numéro du circuit passé en paramètre
    let cirNum = request.params.cirNum

    //On récupère les informations qui lui sont associés afin de préremplir les champs
    async.parallel([
      function (callback){
        model.getListeCircuitInfos( cirNum, function (err, result){callback(null,result)});
      },
      function (callback){
        model.getListeNationalite( function (err, result){callback(null,result)});
      },
    ],
      function (err, result){
        if (err) {
          console.log(err);
          return;
        }
        response.listeCircuitModif = result[0][0];
        response.listeNationaliteModif = result[1];
        response.render('listeCircuit', response); //On renvoie sur listeCircuit
      }
    );
  };

  //Exécution de la requête de modification d'un circuit, puis on renvoie sur la liste des circuits
  module.exports.ModifierCircuitReq = 	function(request, response){
     response.title = 'Répertoire des circuits';

     //On récupère les données du formulaire
     let cirnum = request.params.cirNum;
     let nom = request.body.nom;
     let longueur = request.body.longueur;
     let nat = request.body.nat;
     let img = request.files.upload;
     let imgName = request.files.upload.name;
     let nbSpectateur = request.body.nbSpectateur;
     let description = request.body.description;

     //On upload l'image côté public et admin
     img.mv('../Wroom/public/image/circuit/' + imgName , function(err) {
      if (err)
        console.log(response.status(500).send(err));

      console.log('File uploaded public')
    });
    img.mv('./public/image/circuit/' + imgName , function(err) {
     if (err)
       console.log(response.status(500).send(err));

     console.log('File uploaded admin')
   });

   //On modifie les données présente en base de données
     model.modifCircuit(cirnum, nom, longueur, nat, imgName, nbSpectateur, description, function (err, result) {
         if (err) {
             // gestion de l'erreur
             console.log(err);
             return;
         }
         //console.log(result);
         response.writeHead(301,{Location: '/circuit'});  //On renvoie sur la page des circuits
         response.end();
     }) ;
  }
