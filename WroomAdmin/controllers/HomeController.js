//Home controleur

//Renvoie vers la page notFound
module.exports.NotFound = function(request, response){
    response.title = "Not found";
    response.render('notFound', response);
};
