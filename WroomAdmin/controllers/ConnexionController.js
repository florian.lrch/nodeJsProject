let Cryptr = require('cryptr'); //module permettant le cryptage
let cryptr = new Cryptr('MaSuperCléDeChiffrementDeouF');  //notre clé de chiffrement
let model = require('../models/connexion.js');  //On fait appel au model connexion.js (contient toutes les requêtes SQL)

//Module de connexion
module.exports.Connexion = 	function(request, response){
   response.title = 'Connexion';

   //On récupère les information de login et le mot de passe saisis par l'utilisateur
   let login = request.body.login;
   let mdp = request.body.mdp;

  //On récupère le mot de passe crypté en base de donnée pour le login passé en paramètre
   model.getMdpCrypte(login, function (err, result) {
     if (err) {
         // gestion de l'erreur
         console.log(err);
         return;
     }
    //On vérifie que le login existe bien
    if(result.length != 0){
      let mdpCrypte = result[0]['PASSWD'];  //Si oui, on récupère le mot de passe crypté
      let mdpDecrypte = cryptr.decrypt(mdpCrypte);  //On le décrypt à partir de notre clé de chiffrement

      if (mdpDecrypte == mdp){
        request.session.estConnecte = login;  //Si les mots de passe sont identiques, alors on crée notre variable de session
      }
    }
    response.writeHead(301,{Location: '/pilote'}); //On renvoie sur la liste des écuries
    response.end();  //Enfin on renvoie vers le premier menu de notre application
  }) ;
}


module.exports.Deconnexion = 	function(request, response){
  var login = request.session.login;
  // destruction de la varible de session
  request.session.destroy(function(err) { //On détruit la variable de session
   if (err) {
     console.log(err);
     return;
   }
 });
 response.writeHead(301,{Location: '/'}); //On renvoie sur la liste des écuries
 response.end();
}
