let async = require('async'); //Permet l'éxécution des fonctions de manière asynchrones
let model = require('../models/resultat.js'); //On fait appel au model resultat.js (contient toutes les requêtes SQL)

//Affichage des grands prix
module.exports.AfficherGrandPrix = 	function(request, response){
   response.title = 'Répertoire des résultats';

   model.getListeGrandPrix( function (err, result) {  //On récupère la liste des grands prix
       if (err) {
           // gestion de l'erreur
           console.log(err);
           return;
       }
       response.listeGrandPrix = result;
       //console.log(result);
      response.render('listeResultat', response); //On renvoie le résultat sur listeResultat
  }) ;
}

//Saisie des résultats
module.exports.SaisirResultat = 	function(request, response){
   response.title = 'Saisie des résultats';
   let gpnum = request.body.selectGp;
   async.parallel([
     function (callback){
 			model.getTempsPilote( gpnum, (function (err, result){callback(null,result)}));
 		},
 		function (callback){
 			model.getPlaceNbPoint( function (err, result){callback(null,result)});
 		},
    function (callback){
 			model.getPiloteSansResultat( gpnum, function (err, result){callback(null,result)});
 		},
   ],
     function (err, result){
       if (err) {
         console.log(err);
         return;
       }
   		response.resultats = recupElements();
      response.pilSansResult = result[2];
      response.gpnum = gpnum;
   		function recupElements() {
   			let monTableauObjet = [];
   			let j = 1;
   			let i = 0;
   			while (i < (result[0].length) && i < 10) {
   				monTableauObjet[i] = new Object;
   				monTableauObjet[i].place = result[1][i]['ptplace'];
         monTableauObjet[i].pilnum = result[0][i]['PILNUM'];
   				monTableauObjet[i].pilnom = result[0][i]['PILNOM'];
   				monTableauObjet[i].temps = result[0][i]['TEMPSCOURSE'];
   				monTableauObjet[i].point = result[1][i]['ptnbpointsplace'];
   				i++;
   			}
   			return monTableauObjet;
   		}
       response.render('listeResultat', response);
     }
   );
}

module.exports.SaisirResultatGp = 	function(request, response){
   response.title = 'Saisie des résultats';
   let gpnum = request.params.gpnum;
   async.parallel([
     function (callback){
 			model.getTempsPilote( gpnum, (function (err, result){callback(null,result)}));
 		},
 		function (callback){
 			model.getPlaceNbPoint( function (err, result){callback(null,result)});
 		},
    function (callback){
 			model.getPiloteSansResultat( gpnum, function (err, result){callback(null,result)});
 		},
   ],
     function (err, result){
       if (err) {
         console.log(err);
         return;
       }
   		response.resultats = recupElements();
      response.pilSansResult = result[2];
      response.gpnum = gpnum;
   		function recupElements() {
   			let monTableauObjet = [];
   			let j = 1;
   			let i = 0;
   			while (i < (result[0].length) && i < 10) {
   				monTableauObjet[i] = new Object;
   				monTableauObjet[i].place = result[1][i]['ptplace'];
         monTableauObjet[i].pilnum = result[0][i]['PILNUM'];
   				monTableauObjet[i].pilnom = result[0][i]['PILNOM'];
   				monTableauObjet[i].temps = result[0][i]['TEMPSCOURSE'];
   				monTableauObjet[i].point = result[1][i]['ptnbpointsplace'];
   				i++;
   			}
   			return monTableauObjet;
   		}
       response.render('listeResultat', response);
     }
   );
}

module.exports.AjoutResultat = 	function(request, response){
  response.title = 'Saisie des résultats';
  let gpnum = request.params.gpnum;
  let pilnum = request.body.pilnum;
  function formatHeure(heure, min, sec){
    temps = heure + ":" + min + ":" + sec;
    return temps;
  }
  let heure = request.body.heure;
  let min = request.body.min;
  let sec = request.body.sec;
  let tempsCourse = formatHeure(heure, min, sec); //On convertit nos 3 données pour n'en donner qu'une
  //On ajoute notre pilote à la base de données, puis on récupère les pilotes enregistrés et le nombre de points en fonction du classement
  async.parallel([
    function (callback){
     model.ajoutResultat( gpnum, pilnum, tempsCourse, (function (err, result){callback(null,result)}));
   },
    function (callback){
      model.numDixPremierPilotes(gpnum, (function (err, result){callback(null,result)}));
   },
   function (callback){
     model.getPlaceNbPoint( function (err, result){callback(null,result)});
   },
  ],
    function (err, result){
      if (err) {
        console.log(err);
        return;
      }
      //On initialise nos valeurs
      let nbPil = result[1].length;
      if(nbPil > 10){
        nbPil = 10;
      }
      let sauvegardeEcuPoints = new Object();
      let changementPoint = false;
      let pointsUpdatePil = 0;
      let pointsUpdateEcu = 0;
      let i = 0;
      //On parcours nos 10 premiers pilotes
      while (i < nbPil) {
        //Ce booléen permet de n'effectuer la mise a jour des points à partir du pilote qui a été ajouté
        if (changementPoint == true) {
          pointsUpdatePil = result[1][i]["pilpoints"] - (result[2][i-1]["ptnbpointsplace"] - result[2][i]["ptnbpointsplace"]);  //On détermine les points du pilote
          pointsUpdateEcu = ecuPoints(result[1][i]["ecunum"], i, -(result[2][i-1]["ptnbpointsplace"] - result[2][i]["ptnbpointsplace"])); //On détermine les points de l'écurie
          //On effctue les modification en base de données
          async.parallel([
            function (callback){
             model.modifPointsPil(result[1][i]["pilnum"], pointsUpdatePil, (function (err, result){callback(null,result)}));
           },
            function (callback){
              model.modifPointsEcu(result[1][i]["ecunum"], pointsUpdateEcu, (function (err, result){callback(null,result)}));
           },
          ],
            function (err, result){
              if (err) {
                console.log(err);
                return;
              }
          });
        } else if (result[1][i]["pilnum"] == pilnum){ //Correspont au pilote que l'on a ajouté
            changementPoint = true; //On change le booleen a true pour que les points des prochains pilotes soient mises à jour
            pointsUpdatePil = result[1][i]["pilpoints"] + result[2][i]["ptnbpointsplace"];  //On détermine les points du pilote
            pointsUpdateEcu = ecuPoints(result[1][i]["ecunum"], i, result[2][i]["ptnbpointsplace"]);  //On détermine les points de l'écurie
            //On effctue les modification en base de données
            async.parallel([
              function (callback){
               model.modifPointsPil(result[1][i]["pilnum"], pointsUpdatePil, (function (err, result){callback(null,result)}));
             },
              function (callback){
                model.modifPointsEcu(result[1][i]["ecunum"], pointsUpdateEcu, (function (err, result){callback(null,result)}));
             },

            ],
              function (err, result){
                if (err) {
                  console.log(err);
                  return;
                }

            });
          }
          i++;
      }
      //Si l'on a plus de 10 pilotes enregistrés alors il faut supprimer les points de l'ancien dixième qui se retrouve 11ème
      if(result[1].length > 10){
        pointsUpdatePil = result[1][10]["pilpoints"] - result[2][9]["ptnbpointsplace"]; //On détermine les points du pilote
        pointsUpdateEcu = ecuPoints(result[1][10]["ecunum"], i, -(result[2][9]["ptnbpointsplace"]));  //On détermine les points de son écurie
        //On effectue les modifications en base de données
        async.parallel([
          function (callback){
           model.modifPointsPil(result[1][10]["pilnum"], pointsUpdatePil, (function (err, result){callback(null,result)}));
         },
          function (callback){
            model.modifPointsEcu(result[1][10]["ecunum"], pointsUpdateEcu, (function (err, result){callback(null,result)}));
         },

        ],
          function (err, result){
            if (err) {
              console.log(err);
              return;
            }

        });
      }
      //Fonction qui permet le calcul des points de l'écurie
      function ecuPoints(ecunum, i, diffPoints){  //On passe en paramètre le numéro de l'écurie, l'itération de la boucle et le nombre de points qui doivent être ajouté ou retirer à l'écurie
        if(ecunum != 'null'){ //On vérifie que l'écurie ne soit pas null
          for (let [key, value] of Object.entries(sauvegardeEcuPoints)) { //On utilise notre object qui contient la clé "numéroEcurie" "nombreDePointsEnregistré"
            if(key == ecunum){  //Si on trouve une correspondance entre le numér de l'écurie passé dans la fonction et celui dans notre objet
              let nbpoints = value + diffPoints;  //On récupère la valeur enregistré et on lui fait la modification
              Object.assign(sauvegardeEcuPoints, {[ecunum]: nbpoints}); //Que l'on asigne à notre objet
              return nbpoints;  //Enfin on retourne le nombre de points qui doivent être mise à jour dans notre écurie
            }
          }
           Object.assign(sauvegardeEcuPoints, {[ecunum]: result[1][i]["ecupoints"] + diffPoints});  //Si l'écurie n'existe pas alors on lui crée un clé valeur contenant le numéro de l'écurie et le nombre de points qu'elle a (qu'on a récupéré grace à larequête) avec la différence de points
           return result[1][i]["ecupoints"] + diffPoints; //Enfin on retourne le nombre de points qui doivent être mise à jour dans notre écurie
        } else {
          return 0;
        }
      }
      response.writeHead(301,{Location: '/saisirResultat/' + gpnum});  //Enfin on renvoie sur la page des résultats
      response.end();
    }
  );
};


module.exports.SuppResultat = 	function(request, response){
  response.title = 'Saisie des résultats';
  let gpnum = request.body.gpnum;
  let pilnum = request.body.pilnum;

  //Récupère dans un premier temps les pilotes du grand prix et le nombre de points par place au classement
  async.parallel([
    function (callback){
      model.numDixPremierPilotes(gpnum, (function (err, result){callback(null,result)}));
    },
    function (callback){
      model.getPlaceNbPoint( function (err, result){callback(null,result)});
    },
  ],
  function (err, result){
    if (err) {
      console.log(err);
      return;
    }

    //Initialisation des variables
    let nbPil = result[0].length;
    if(nbPil > 10){
      nbPil = 10;
    }
    let changementPoint = false;
    let sauvegardeEcuPoints = new Object();
    let pointsUpdatePil = 0;
    let pointsUpdateEcu = 0;
    let i = 0;
    console.log(sauvegardeEcuPoints);
    //Pour les 10 premiers pilotes on effectue le changement de points d'écurie et de pilote
    while (i < nbPil) {
      //Ce booléen permet de n'effectuer la mise a jour des points à partir du pilote qui est supprimé
      if(changementPoint == true){
        pointsUpdatePil = result[0][i]["pilpoints"] + (result[1][i - 1]["ptnbpointsplace"] - result[1][i]["ptnbpointsplace"]);  //On calcule les points du pilote
        pointsUpdateEcu = ecuPoints(result[0][i]["ecunum"], i, (result[1][i - 1]["ptnbpointsplace"] - result[1][i]["ptnbpointsplace"]));  //On calcule les points de l'écurie
        //On éxécute les requêtes
        async.parallel([
          function (callback){
           model.modifPointsPil(result[0][i]["pilnum"], pointsUpdatePil, (function (err, result){callback(null,result)}));
         },
         function (callback){
          model.modifPointsEcu(result[0][i]["ecunum"], pointsUpdateEcu, (function (err, result){callback(null,result)}));
        },
        ],
          function (err, result){
            if (err) {
              console.log(err);
              return;
            }

        });
      } else if (pilnum == result[0][i]["pilnum"])  { //Quand on a le pilote a supprimer
        changementPoint = true; //On permet la modification de points des pilotes le succédant au classement
        pointsUpdatePil = result[0][i]["pilpoints"] - result[1][i]["ptnbpointsplace"];  //On calcule les points du pilote
        console.log("YoYo");
        console.log(-(result[1][i]["ptnbpointsplace"]));
        pointsUpdateEcu = ecuPoints(result[0][i]["ecunum"],i, -(result[1][i]["ptnbpointsplace"]));  //On calcule les points de l'écurie
        //On éxécute les requêtes
        async.parallel([
          function (callback){
           model.modifPointsPil(result[0][i]["pilnum"], pointsUpdatePil, (function (err, result){callback(null,result)}));
         },
         function (callback){
           model.modifPointsEcu(result[0][i]["ecunum"], pointsUpdateEcu, (function (err, result){callback(null,result)}));
        },
          function (callback){
            model.supp(gpnum, pilnum, (function (err, result){callback(null,result)})); //Et on supprime le pilote
         },
        ],
          function (err, result){
            if (err) {
              console.log(err);
              return;
            }

        });

      }
      console.log(sauvegardeEcuPoints);
      i++;
    }
    //Si le tableau des résultats contient plus de 10 pilotes, il ne faut pas oublier de mettre à jour les points du 11ème qui se retrouve 10ème suite à la suppression
    if(result[0].length > 10){
      pointsUpdatePil = result[0][10]["pilpoints"] + result[1][9]["ptnbpointsplace"];
      pointsUpdateEcu = ecuPoints(result[0][10]["ecunum"], 10, result[1][9]["ptnbpointsplace"]);
      async.parallel([
        function (callback){
         model.modifPointsPil(result[0][10]["pilnum"], pointsUpdatePil, (function (err, result){callback(null,result)}));
       },
        function (callback){
          model.modifPointsEcu(result[0][10]["ecunum"], pointsUpdateEcu, (function (err, result){callback(null,result)}));
       },

      ],
        function (err, result){
          if (err) {
            console.log(err);
            return;
          }

      });
    }

    //Fonction permettant le calcul des points des écurie
    function ecuPoints(ecunum, i, diffPoints){  //On passe en paramètre le numéro de l'écurie, l'itération de la boucle et le nombre de points qui doivent être ajouté ou retirer à l'écurie
      if(ecunum != 'null'){ //On vérifie que l'écurie ne soit pas null
        for (let [key, value] of Object.entries(sauvegardeEcuPoints)) { //On utilise notre object qui contient la clé "numéroEcurie" "nombreDePointsEnregistré"
          if(key == ecunum){  //Si on trouve une correspondance entre le numér de l'écurie passé dans la fonction et celui dans notre objet
            let nbpoints = value + diffPoints;  //On récupère la valeur enregistré et on lui fait la modification
            Object.assign(sauvegardeEcuPoints, {[ecunum]: nbpoints}); //Que l'on asigne à notre objet
            return nbpoints;  //Enfin on retourne le nombre de points qui doivent être mise à jour dans notre écurie
          }
        }
         Object.assign(sauvegardeEcuPoints, {[ecunum]: result[0][i]["ecupoints"] + diffPoints});  //Si l'écurie n'existe pas alors on lui crée un clé valeur contenant le numéro de l'écurie et le nombre de points qu'elle a (qu'on a récupéré grace à larequête) avec la différence de points
         return result[0][i]["ecupoints"] + diffPoints; //Enfin on retourne le nombre de points qui doivent être mise à jour dans notre écurie
      } else {
        return 0;
      }
    }
    response.writeHead(301,{Location: '/saisirResultat/' + gpnum});  //Enfin on renvoie sur la page des résultats
    response.end();
  }
);
};
