//Répertoire des écuries
let async = require('async'); //Permet l'éxécution des fonctions de manière asynchrones
let model = require('../models/ecurie.js'); //On fait appel au model ecurie.js (contient toutes les requêtes SQL)

//On récupère la liste de nos écuries
module.exports.ListeEcurie = 	function(request, response){
   response.title = 'Répertoire des écuries';
   //On appelle la fonction getListeEcurie
   model.getListeEcurie( function (err, result) {
       if (err) {
           console.log(err);
           return;
       }
       response.listeEcurie = result; //On met le résultat dans listeEcurie

      response.render('ListeEcurie', response); //On renvoie vers la page ListeEcurie
  }) ;

}

//Page de l'ajout d'une écurie
module.exports.AjoutEcurie = 	function(request, response){
   response.title = 'Ajouter une écurie';

   model.getListeNationalite( function (err, result) {  //On récupère la liste des nationalités à afficher dans le formulaire
       if (err) {
           console.log(err);
           return;
       }
       response.listeNationalite = result;  //On met le résultat dans listeNationalite

      response.render('listeEcurie', response); //On renvoie le résultat vers listeEcurie
  }) ;
}

//Ajout d'une écurie en base de données
module.exports.AjoutEcurieReq = 	function(request, response){
   response.title = 'Répertoire des écuries';

   //On récupère toutes les informations du formulaire
   let nom = request.body.nom;
   let directeur = request.body.directeur;
   let adr_siege = request.body.adr_siege;
   let nb_points = request.body.nb_points;
   let nat = request.body.nat;
   let img = request.files.upload;
   let imgName = request.files.upload.name;

   //On upload l'image dans nos fichiers côté client et côté serveur
   img.mv('../Wroom/public/image/ecurie/' + imgName , function(err) {
    if (err)
      console.log(response.status(500).send(err));

    console.log('File uploaded public')
  });
  img.mv('./public/image/ecurie/' + imgName , function(err) {
   if (err)
     console.log(response.status(500).send(err));

   console.log('File uploaded admin')
  });
    //On insert toutes les données de notre nouvelle écurie dans la base de données
   model.insertEcurie(nom, directeur, adr_siege, nb_points, nat, imgName, function (err, result) {
       if (err) {
           // gestion de l'erreur
           console.log(err);
           return;
       }
       //console.log(result);
       response.writeHead(301,{Location: '/ecurie'}); //On renvoie sur la page des écuries
       response.end();
   }) ;
}

//Suppression d'une écurie
module.exports.SupprimerEcurie = 	function(request, response){
   response.title = 'Supprimer une écurie';
   let ecuNum = request.params.ecuNum;  //On récupère le numéro de l'écurie que l'on souhaite supprimer
   //On supprime les écuries et les tables qui lui sont associées
   async.parallel([
     function (callback){
       model.suppFinance( ecuNum, function (err, result){callback(null,result)});
     },
     function (callback){
       model.modifEcuPilote( ecuNum, (function (err, result){callback(null,result)}));
     },
     function (callback){
       model.suppEcu( ecuNum, (function (err, result){callback(null,result)}));
     },
   ],
     function (err, result){
       if (err) {
         console.log(err);
         return;
       }

       // response.writeHead(301,{Location: '/ecurie'}); //On renvoie sur la page des écuries
       // response.end();
       response.render('ecurie', response); //On renvoie le résultat vers listeEcurie
     }
   );
 };

 //Page de modification d'une écurie
 module.exports.ModifierEcurie = 	function(request, response){
    response.title = 'Modifier une écurie';
    let ecuNum = request.params.ecuNum; //On récupère le numéro de l'écurie passé en paramètre de l'URL

    //On récupère les informations relatives à l'écurie
    async.parallel([
      function (callback){
        model.getListeEcurieInfos( ecuNum, function (err, result){callback(null,result)});
      },
      function (callback){
        model.getListeNationalite( function (err, result){callback(null,result)});
      },
    ],
      function (err, result){
        if (err) {
          console.log(err);
          return;
        }

        response.listeEcurieModif = result[0][0];
        response.listeNationaliteModif = result[1];
        response.render('listeEcurie', response); //On renvoie le résultat vers listeEcurie
      }
    );
  };

  //Modification de l'écurie
  module.exports.ModifierEcurieReq = 	function(request, response){
     response.title = 'Répertoire des écuries';

     //On récupère toutes les informations passés dans notre formulaire
     let ecunum = request.params.ecuNum;
     let nom = request.body.nom;
     let directeur = request.body.directeur;
     let adr_siege = request.body.adr_siege;
     let nb_points = request.body.nb_points;
     let nat = request.body.nat;
     let img = request.files.upload;
     let imgName = request.files.upload.name;

     //On upload l'image côté public et côté serveur
     img.mv('../Wroom/public/image/ecurie/' + imgName , function(err) {
      if (err)
        console.log(response.status(500).send(err));

      console.log('File uploaded public')
    });
    img.mv('./public/image/ecurie/' + imgName , function(err) {
     if (err)
       console.log(response.status(500).send(err));

     console.log('File uploaded admin')
    });

    //On éxécute notre requête de modification de notre écurie
     model.modifEcurie(ecunum, nom, directeur, adr_siege, nb_points, nat, imgName, function (err, result) {
         if (err) {
             // gestion de l'erreur
             console.log(err);
             return;
         }
         //console.log(result);
         response.writeHead(301,{Location: '/ecurie'}); //On renvoie sur la liste des écuries
         response.end();
     }) ;
  }
