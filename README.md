# Projet NodeJS

Projet universitaire - Création d'un serveur WEB sous NodeJS

Structure du projet :

![Struture du projet](/structure_capture.PNG)

## Technologies utilisées

### Langages

* HTML
* CSS
* JavaScript

### Framework

* Express

### Moteur de template

* Handlebars

### Base de données

* MySQL

## Installation 

Le fichier grandprix.sql vous permettra de créer votre base grandprix.

Pour démarrer le serveur, taper "node app".  
Le serveur sera à l'écoute du port 6800.  
Le fichier config.db contient les paramètres pour la base MySQL.  

## Capture d'écran

Interface public

![Interface public](/public_capture.PNG)

Interface administration

![Interface administration](/admin_capture.PNG)